#!/usr/bin/env python
# -*- coding:utf-8 -*-
u'''
Dyanamic DNS Updater

@copyright: TIELabo.com
@license: MIT
@requires: dnspython
'''

__author__ = "masaki <tielabo@exleaf.jp>"
__status__ = "production"
__version__ = "0.0.1"
__date__    = "2013/01/30"

import urllib, re
import socket
import dns.reversename
import dns.name
    
class DDNSClient(object):
    u'''
    Dyanamic DNS Update Client
    '''


    def __init__(self, ip_check_url, dns_address, update_query_url, host, domain, user, passwd):
        u'''
        Constructor
        @param ip_check_url: グローバルIPアドレス取得URL
        @param dns_address: DNSチェックサーバー
        @param update_query_url: Dynamic DNS更新用QueryURL
        @param host: 対象ホスト
        @param domain: 対象ドメイン
        @param user: ログインユーザー
        @param passwd: ログインパスワード
        '''

        self._ip_check_url = ip_check_url
        self._dns_address = dns_address
        self._update_query_url = update_query_url
        self._host = host
        self._domain = domain
        self._user = user
        self._passwd = passwd

        self._global_ip = None


    def update(self):
        u'''
        Dynamic DNS 更新処理
        IPアドレスが変わっていなければ変更しない
        登録失敗で例外発生
        '''
        self._checkip()

        if self._is_dns_match():
            return

        return self._update_ddns()



    def get_global_ip(self):
        u'''
        グローバルIPアドレスを取得する
        '''
        r = re.compile(u'(?P<ip>\d+\.\d+\.\d+\.\d+)')
        ip = urllib.urlopen(self._ip_check_url).read()
        ip = r.search(ip).groupdict()['ip']

        self._global_ip = ip
        return ip


    def _checkip(self):
        u'''
        グローバルIPのチェック
        '''
        if self._global_ip == None:
            self.get_global_ip()


    def _is_dns_match(self):
        u'''
        DNS検証をして、現在のIPアドレスと同じかどうか
        '''
        host = ""
        if self._host == None or len(self._host) == 0 or self._host == '*':
            pass
        else:
            host = self._host + '.'
        host += self._domain


        retip = self._global_ip
        if (self._dns_address == None) or (len(self._dns_address) == 0):
            retip = socket.gethostbyname(host)
        else:
            tmp = dns.reversename.ipv4_reverse_domain
            try:
                tmp2 = dns.name.from_text(self._dns_address)
                dns.reversename.ipv4_reverse_domain = tmp2
                retip = dns.reversename.to_address(host)
            finally:
                dns.reversename.ipv4_reverse_domain = tmp
                
        return retip == self._global_ip


    def _update_ddns(self):
        u'''
        Dynamic DNSにIPを更新するQueryを発行する
        '''

        query = self._update_query_url
        # 各辞書の部分を置換する
        url = str(query).format(IP= self._global_ip, HOST= self._host, DOMAIN= self._domain, USER= self._user, PASS= self._passwd)
        response = urllib.urlopen(url).read()

        return response