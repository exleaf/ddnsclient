#!/usr/bin/env python
# -*- coding:utf-8 -*-
u'''
Dyanamic DNS Updater

@copyright: TIELabo.com
@license: MIT
'''

import sys, os
import ConfigParser

import ddns.DDNSClient

if __name__ == '__main__':
    setting = ConfigParser.SafeConfigParser()
    setting.read(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'setting.ini'))
    sec = 'settings'

    ip_check_url = setting.get(sec, 'ip_check_url')
    dns_address = setting.get(sec, 'dns_address')
    update_query_url = setting.get(sec, 'update_query_url')
    host = setting.get(sec, 'host')
    domain = setting.get(sec, 'domain')
    user = setting.get(sec, 'user')
    passwd = setting.get(sec, 'passwd')
    
    client = ddns.DDNSClient.DDNSClient(ip_check_url, dns_address, update_query_url, host, domain, user, passwd)
    
    ret = client.update()
    if ret == None:
        sys.exit(0)
